Hi [Opfer],

you asked for your Debian Account to be reactivated because it is in the
emeritus state.  For various reasons[1] we no longer simply reactivate
the account.  Instead we ask you to answer a few questions about P&P and
T&S to check that you still know the basics which are needed as a Debian
Developer.
Note: This is not the full-blown set of Questions used in the New
Maintainers' Process, but if you wish we can also use that set. :-)

[1] https://lists.debian.org/debian-devel-announce/2005/02/msg00003.html

The first thing you need to provide is of course, a GPG key.  If you
still use the one you had at the time you left, send me a signed reply
to this mail to prove that you still control it.  If you are now using a
new key, you need to have it signed by at least 2 existing Developers.

Also, if you haven't done so already, please tell us the name of the
account you had to make life easier for us.

As I'm always curious, a not so important question in this mail - why
did you leave the project, and why are you now trying to come back?

One word left before the important things start: As this is a rather
long mail please use proper quoting in your answer.  Please don't take
this personally: as DAM I have to deal with many bad examples, which
makes my life harder than necessary.

OK, here we start with P&P, checking your knowledge of Debian policy and
procedures.  In preparation, you should read the Debian constitution[1],
Social Contract[2] and the Free Software Guidelines, and finally the
developer's reference[3] and the current version of Debian policy[4].

URLs:
  [1] https://www.debian.org/devel/constitution
            Constitution for the Debian Project
  [2] https://www.debian.org/social_contract.en.html
            Debian Social Contract
  [3] https://www.debian.org/doc/developers-reference/
            Debian Developer's Reference
  [4] https://www.debian.org/doc/debian-policy/
            Debian Policy Manual

After you have done this, please answer the following set of questions
and try to be quite verbose in your answers.


First, please explain the key points of the Social Contract and the DFSG
_in your own words_.  Also, describe what you personally think about
these documents.

 0. What is Debian's approach to non-free software?  Why?  Is non-free
    part of the Debian System?

 1. Suppose that Debian were offered a Debian-specific license to
    package a certain piece of software: would we put it in main?

 2. Are there any sections of the DFSG or Social Contract that you might
    like to see changed?  If so, which ones, and why?


Do you agree to uphold the Social Contract and the DFSG in your Debian
work?


If I'm satisfied with your answers, you will get your accounts on the
Debian machines reactivated.  I'm sure you agreed to the DMUP before you
initially got your account, but please reaffirm that here.

Have you read the Debian Machine Usage Policies (DMUP) at
https://www.debian.org/devel/dmup ?  Do you accept them?


 3. What are Non-Maintainer Uploads (NMUs) and when would you do an NMU?
    Please list the usual procedure for a NMU.

 4. Please tell me 3 different methods to close a bug in the BTS, the
    difference between them, and when to use which method.

 5. You just discovered a bug in many packages.  What are your next
    steps?

 6. How do you check a package before you upload?  Please explain to me
    why and how you perform the checks.

 7. There are many Debian suites, like "stable", "unstable", "testing",
    "jessie", "stretch" and "sid".  Can you explain why there are so many
    and what the differences are?  How does a package get from one to
    the other?  What is special with experimental?  Does frozen exist?
    How do bugs and their tags and severities affect the release status
    of a debian package?

 8. Imagine you maintain a package which depends very closely to some
    other package.  How would you keep track of the development of other
    packages, even if you are not the maintainer?

 9. If you had a file in your package which usually gets changed by a
    administrator for local settings, how do you make sure your next
    version of the package doesn't overwrite it?

10. What is an autobuilder?  Where can you find information about your
    package's build status on different architectures?  What can you do
    if you think there is a problem with the autobuilder?

11. Should you happily sign another developer's GPG key?  If not,
    please explain the checks you will make before signing it.

12. What do you do if you want to reach the submitter of a bug and keep
    a copy of the mail in the BTS?

13. Please list some tasks that belong to the scope of duties of the
    Debian Quality Assurance group.

14. What does the version string in the Standards-Version field of a
    package's control file represent?  Why is it useful?

15. You can't/wont maintain a package properly anymore because you have
    a lack of time/don't use it anymore.  What are your options to
    handle this situation?


That is the P&P part, the next set of questions below asks about T&S,
have fun.

 0. What is the best way to check that your Build-Depends Line contains
    everything required to build your package?

 1. What does it mean for a package to have a line like "Architecture:
    i386 alpha powerpc" in the control file?  Do you have to provide
    binary packages for all these architectures if you upload your
    package?  What is the difference between "Architecture: all" and
    "Architecture: any"?

 2. Where can you find more information about how your package should
    look like?  Please list as many places as you know.

 3. What is the difference between native packages and non-native
    packages?

 4. How does Debian handle packages that need to run some
    initialization every time the system is booted?  How does this
    interact with users' local modifications?  What should packages
    which require starting up and shutting down do (think about
    daemons as an example)?  How can you minimize the downtime of a
    service during upgrade?  Be specific about how you'd use the
    maintainer scripts. What would you add regarding support for
    different init systems?

 5. What would you need to do if upstream changed the name of the
    application?  What would you do to assure smooth upgrades?

 6. What would you do if a package has no sane default configuration?
    (There is *no* default configuration that works on most systems!)

 7. What is endianess and why does it matter when supporting multiple
    architectures?

 8. If you want to sponsor a package upload, what do you need to do?

Ok, I'm finished with the questions.  The following is a summary about
important mailing lists from the project, packages which one should at
least know about and a few links to important sites which can help to
answer the questions if you get in trouble with one of them.


A word on mailing lists: there are quite a lot of Debian mailing lists
now as well as packaging-related packages, and I'd just like to check
with you whether you know about the most important of these.

  debian-announce: Major public announcements
  debian-devel-announce: Major announcements to the developer community

These two lists are must-subscribes.  Everything else is optional.  I
abbreviate 'debian-' to '-' from now on.

  -security-announce: security updates to stable
  -private: you'll be re-subscribed automatically when the account is
            re-activated (but you can unsubscribe if you wish);
            the list is used for sensitive discussions, etc.
  -devel:   general mailing list for developer issues
  -policy:  where possible changes to debian-policy are discussed
  -mentors: helping newbie Developers.
  -project: project related discussions

There are many others; check the mailing list page on the web site
for details.


Now let's take a look at some important packages for a Debian
Developer.  There are many of them, I will try to list the more important
ones.

  build-essential
            A package that depends on all the packages in the build
            essential list.  It's useful to make sure everything in the
            list is installed on the system when building and testing
            your own packages.
  dpkg-dev  All of the primary tools needed to put a Debian package
            together: dpkg-buildpackage, dpkg-source, etc.
  debhelper A very useful set of scripts designed to make debian/rules
            files more readable and uniform.  But you should be able to
            build a package without it.
  debian-policy
            Describes the policy relating to packages and details of the
            packaging mechanism.  Covers everything from required gcc
            options to the way the maintainer scripts (postinst etc.)
            work, package sections and priorities, etc.  An absolute
            must-read.  Also useful is the file
            /usr/share/doc/debian-policy/upgrading-checklist.txt.gz,
            which lists changes between versions of policy.  You must
            read and understand it.
  doc-debian
            Lots of useful Debian-specific documentation: the
            constitution and DFSG, explanation of the Bug Tracking
            System (BTS), etc.
  maint-guide
            The New Maintainer's Guide to making Debian packages.
  devscripts
            Lots of useful (and not-so-useful) scripts to help build
            packages.
  developers-reference
            Lots of information on procedures and suchlike.
  dupload or dput
            Automatically upload packages to the archive once they are
            built.
  fakeroot  Build packages without having to be root.
  reportbug Tool to report bugs.
  debootstrap
            Allows you to "install" Debian's base on a given directory
            anywhere on the filesystem.  Combined with a chroot and
            build-essential, this makes for a nice way to have a clean
            environment where you can build your packages.
  pbuilder  Gives you an easy way to use debootstrap to test your
            packages in a sane environment
  sbuild    Tool to build your packages in a chroot (useful for
            verifying build-deps)
  lintian   Package to check your package for commonly made errors.  You
            should never upload a package which is not checked by this
            tool.
  dpatch
  quilt     Three packages that help you to manage multiple patches to
  cdbs      your package in a sane way.


Finally, some important web links:

  https://www.debian.org/devel/
            The Developer's Corner.  Contains links and on-line versions
            of the stuff I mentioned before.
  https://db.debian.org/
            Queries about developers and machines
  https://www.debian.org/devel/wnpp/
            The Work Needing and Prospective Packages list.  Sort of a
            big TODO list for Debian packaging stuff: what's orphaned,
            what needs new maintainers, what's being adopted, what's
            being packaged and what would be nice to have packaged.
  https://qa.debian.org/
            The Debian Quality Assurance headquarters.  Help is
            appreciated!
  https://bugs.debian.org/
            Bug related info
  https://www.debian.org/security/
            Security related info.  Please, read the FAQ, as it will
            save you (and others) a lot of headaches
  https://packages.debian.org/
            Package related info
  https://buildd.debian.org/
            Build status of Debian packages
  https://lists.debian.org/
            Mailing list subscription and archives
  https://qa.debian.org/developer.php
            An interesting place to keep track of your packages.
  https://lintian.debian.org/
            Automated lintian tests on all packages in the Debian
            Archive.
  https://tracker.debian.org/
            The Debian Package Tracker
  https://wiki.debian.org/WritingDebianPackageDescriptions
            A guide on writing package descriptions
  https://people.debian.org/~bap/dfsg-faq.html
            DFSG and Software License FAQ
