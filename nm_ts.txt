
Lets go to the next big part of your application - the check of Tasks
and Skills. Similar to P&P it is also split in two steps: a set of
questions and finally a thorough check of your packages.

If you have work to do on your packages, now is a good time to do it.
I will sponsor uploads for you if you like. This of course helps the
package check, but will also allow me to see how you handle the things
we are talking about here in practice.

<!--
Dear NM applicant,

if you can read this, it means that your AM being lazy and posting all of the
templates, instead of asking only those questions that they think should be
asked in your case.

They probably do not know that they can use DDPortfolio[1] or minechangelogs[2]
to see what you already know, and save themselves the burden of checking long
boring messages with replies to lots of answers.

Please quote this bit to them, so they learn :)

[1] https://wiki.debian.org/DDPortfolio
[2] https://wiki.debian.org/Services/minechangelogs
[3] https://wiki.debian.org/FrontDesk/Tips
-->

Here are the questions:


Bug Fixing
----------

(Feel free to reply to this section in a separate message, as bug
fixing and developing a good patch may use some time.)

RC1. Have you ever written a manpage? If so, please tell me which. If
     not, please run manpage-alert or take a look at
     https://qa.debian.org/man-pages.html and choose a package listed
     there. Then please write a manpage for it, submit it to the BTS
     and tell me the bug number for it.

RC2. Please find a bug to fix. For example you can look into rc-alert,
     wnpp-alert or
     https://bugs.debian.org/release-critical/debian/all.html It does
     not need to be a difficult bug to fix, but it is a good idea to
     find a bug older than a few days.

     Try to create a patch to fix it and submit this patch to the BTS.
     If you can't fix the bug, you should document what you've found
     out about the bug in the BTS.  Then prepare, if possible, a NMU
     and send me a pointer to your NMU patch.


Debian Package Format
---------------------

PF1. What does version 2:3.4~rc1-2.1+deb7u1 mean? What Debian control
     file would you put this in?

PF2. What does the version string in the Standards-Version field of a
     package's control file represent? Why is it useful?

PF5. How do you choose the "urgency" field in the changelog?

PF7. What is Essential: yes? Why isn't libc essential and why can't it
     be? Why does it not need to be essential? Why isn't the kernel
     essential? Should init be essential?

PFf. Write a small shell script which does the following two things:
     a. prints whether a Debian binary package has a copyright file
        in the appropriate location.
     b. prints out the package version from the control file which is
        inside the .deb.
     You may use tar, ar, grep, etc., but not any middle or high-level
     dpkg tools.


Practical packaging
-------------------

If you have filed bugreports (with patches) to the BTS with respect to
packaging issues, please tell me bug numbers, so I can check them.

PP4. If one of your packages had serious problems like either
       a) the current version in the archive is not "mature" enough
          to be in a Debian release but it is developing and you still
          want to maintain it
       b) the software was abandoned upstream or became obsolete for
          other reasons and you consider it not worth including in
          Debian.
      How would you proceed in these cases?

PP9. What would you do if a package has no sane default configuration?
     Suppose that it is impossible to create a default configuration that
     works on most systems.

PPc. Run: "dpkg -L devscripts | grep bin/ | shuf -n10" and have a look
     at the manpage of those scripts that you've never heard of.
     If you stumble on some script that does not make sense or into an
     option that is particularly obscure, please ask me about it and I
     will try to find out what it is for.


Package Building and Uploading
------------------------------

BU1. How do you manage new upstream releases?

BU5. If you want to sponsor a package upload, what do you need to do?
     Please take a random package from the archive and send me the
     .changes file as it would look if you were sponsoring an upload of
     this package.

BU6. How do you upload a package to
     - unstable
     - stable / stable-proposed-updates (is there a difference?)
     - the DELAYED queue

BU7. What do you do if you've uploaded broken or incomplete files to
     the upload queue (something that can easily happen if your internet
     connection is not stable, for example)?


Architectures and Libraries
---------------------------

AL5. What target would you use in debian/rules to build a .deb
     package which includes only non-architecture-dependent files?
     What is the "Architecture:" field for this package?

AL7. Why does a libfoo-dev package depend on libfooX?
     Why is it libfooX-dev and not libfoo-dev in some cases?
     (When) is libfooX-dev preferable over libfoo-dev?

ALA. What are debian/*.symbols files? How are they maintained?


That's the end of the questions. I will check your packages when we are
finished here, so please upload the latest and greatest packages to the
archive or tell me where I can find them.
