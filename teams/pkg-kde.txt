NM questions about the pkg-kde team work

P&P

 KDEP1. What is KDE?

 KDEP2. What's the recommended way of tagging in pkg-kde git repositories?

T&S

 KDET1. Some library packages maintained by Debian Qt/KDE Team is having a "a",
        "b" or "c "suffix. What is the reason for this?

 KDET2. What's pkgkde-symbolshelper and what does it do?

 KDET3. Some packages have been failing with errors like:

          error: no matching function for call to 'qMin(qreal, double)'

	on some architectures. What is the reason for this?

        How can it be fixed?

 KDET4. What are dh_sodeps and dh_sameversiondep doing?


